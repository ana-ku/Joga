<?php get_header(); ?>

<!-- intro -->
<section id="intro" class="text-center align-center">
    <div class="intro-centered">
        <h1 class="red-color"><?= get_field('intro_sekce_uvodni_veta_intro'); ?></h1>
        <a href="#" class="button sw-button-tertiary sw-continue" aria-hidden="true"><?= get_field('intro_sekce_pokracovat_tlacitko_intro'); ?></a>
    </div>
    <div class="intro-background"></div>
</section><!-- /#intro -->

<!-- mission -->
<section id="mission" class="text-center align-center sw-line">
    <header>
        <h1 class="red-color"><?= mb_strtoupper( get_field('poslani_sekce_poslani_nadpis') ); ?></h1>
    </header>
    
    <div class="row narrow mission-content align-center">
        
        <?= get_field('poslani_sekce_poslani_text'); ?>
        
        <div class="sw-signature-small">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/podpis.jpg" alt="Podpis Daniel Siwy">
            <span class="sw-sign-small">Daniel Siwy</span>
        </div>
        
        <a href="<?= get_field('poslani_sekce_poslani_tlacitko_odkaz'); ?>" class="button sw-button sw-section-button"><?= get_field('poslani_sekce_poslani_tlacitko'); ?></a>
        
        <div class="sw-signature">
            <span class="sw-sign">Daniel Siwy</span>
        </div>
    </div>
</section><!-- /#mission -->

<?php // Get services
$args_services = array(
    'post_type'		=> SERVICES_PT,
    'post_status'	=> 'publish',
    'order'         => 'ASC',
    "posts_per_page"=> -1
);
$services = new WP_Query($args_services); ?>

<?php if ( $services->have_posts() ) : ?>
    <?php $the_last = end($services->posts); ?>
    <!-- services -->
    <section id="services" class="text-center align-center sw-line">
        <header>
            <h1 class="red-color"><?= get_field('specializace_sekce_specializace_nadpis'); ?></h1>
        </header>
    
        <div class="univerzal-overlay"></div>
        
        <div class="row">
            <?php $index = 0; ?>
            <?php while ( $services->have_posts() ) : $services->the_post(); $index++; ?>
                <div class="small-12 medium-6 large-6 columns <?php if(get_the_ID()!=$the_last->ID) { echo ($index % 2)>0 ? 'text-right' : 'text-left'; } else { echo 'grid-centered'; } ?>">
                    <a href="<?php the_permalink(); ?>" class="button sw-button-secondary"><?php !empty(get_field('text_tlacitka')) ? the_field('text_tlacitka') : the_title(); ?></a>
                </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
        
        <footer>
            <a href="<?= get_field('specializace_sekce_specializace_tlacitko_odkaz'); ?>" class="button sw-button sw-section-button"><?= get_field('specializace_sekce_specializace_tlacitko'); ?></a>
        </footer>
    </section><!-- /#services -->
<?php endif; ?>

<?php // Get lawyers
$args_lawyers = array(
    'post_type'		=> LAWYER_PT,
    'post_status'	=> 'publish',
    'order'         => 'ASC',
    "posts_per_page"=> -1
);
$lawyers = new WP_Query($args_lawyers); ?>

<?php if ( $lawyers->have_posts() ) : ?>
    <!-- team -->
    <section id="team" class="text-center align-center sw-line">
        <header>
            <h1 class="red-color"><?= get_field('tym_sekce_tym_nadpis'); ?></h1>
        </header>
        
        <div class="row narrow members-row align-center">
            <?php while ($lawyers->have_posts()): $lawyers->the_post(); ?>
                <div class="member extra-small-1 small-6 medium-3 large-3 columns member-photo">
                    <a href="<?= esc_url( get_page_link(47) . '#' . sanitize_title(get_the_title(), '') ); ?>" class="sw-link">
                        <?php if(has_post_thumbnail()) : ?>
                            <div class="photo-elem sw-grayscale">
                                <?php the_post_thumbnail('thumbnail'); ?>
                            </div>
                        <?php endif; ?>
                        <span class="member-name"><?php the_title(); ?></span>
                    </a>
                </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
        
        <footer>
            <a href="<?= get_field('tym_sekce_tym_tlacitko_odkaz'); ?>" class="button sw-button sw-section-button"><?= get_field('tym_sekce_tym_tlacitko'); ?></a>
        </footer>
    </section><!-- /#team -->
<?php endif; ?>

<?php // Get news	
$args_news = array(
    'post_type'		=> 'post',
    'post-status'	=> 'publish',
    "posts_per_page"=> 10
);
$news = new WP_Query($args_news);
$i = 0; ?>
<?php if ( $news->have_posts() ) : ?>
    <!-- news -->
    <section id="news" class="text-center align-center sw-line">
    <header>
        <h1 class="red-color"><?= get_field('novinky_sekce_novinky_nadpis'); ?></h1>
    </header>
    
    <div class="row narrow">
        <div class="orbit" role="region" aria-label="<?php _e('Current information','grafiquex'); ?>" data-orbit>
            <div class="orbit-wrapper">
                <div class="orbit-controls">
                    <button class="orbit-previous sw-horiz-rot" title="<?php _e('Previous article','grafiquex') ?>"></button>
                    <button class="orbit-next" title="<?php _e('Next article','grafiquex') ?>"></button>
                </div>
                <ul class="orbit-container">
                    <?php while ( $news->have_posts() ) : $news->the_post(); $i++; ?>
                        <li class="orbit-slide <?php echo $i==1 ? 'is-active' : ''; ?>">
                            <div class="row">
                                <div class="date-column small-12 medium-2 large-2 columns">
                                    <span class="date"><?php echo get_the_date('j/n'); ?><br><?php echo get_the_date('Y'); ?></span>
                                </div>

                                <div class="content-column small-12 medium-10 large-10 columns">
                                    <a href="<?php the_permalink(); ?>" class="reference-row news-row" aria-label="<?= __('Detail of the post: ', 'grafiquex') . get_the_title(); ?>"><h4><?php the_title(); ?></h4></a>
                                    <div class="entry-content"><?php echo the_excerpt(); ?></div>
                                    <p class="entry-content entry-content-mobile"><?php echo fmwp_limit_given_string(get_the_content(),' ',200,'...<a href="'.get_the_permalink().'" class="red-color">'.__('continue','grafiquex').'</a>'); ?></p>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
    </div>
    
    <footer>
        <a href="<?= get_field('novinky_sekce_novinky_tlacitko_odkaz'); ?>" class="button sw-button sw-section-button"><?= get_field('novinky_sekce_novinky_tlacitko'); ?></a>
    </footer>
</section><!-- /#news -->
<?php endif; ?>

<?php get_footer(); ?>