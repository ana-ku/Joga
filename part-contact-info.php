<?php
/**
 * Template Part: Contact info
 */
?>

<div class="row">
    <div class="member-photo small-5 medium-5 large-5 sw-v-rcentered">
        <?php if(has_post_thumbnail()) : ?>
            <!--  Lawyer thumbnail  -->
            <div class="photo-elem">
                <?php the_post_thumbnail('thumbnail'); ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="member-info small-7 medium-7 large-7 text-left sw-v-rcentered">
        <!--  Lawyer info  -->
        <div class="">
            <h4 class="member-full-name">
                <a href="<?= esc_url( get_page_link(47) . '#' . sanitize_title(get_the_title(), '') ); ?>" class="sw-link red-color">
                    <span class="member-degree"><?php /*echo !empty($lawyer_info['titul_pred_jmenem'][0]) ? $lawyer_info['titul_pred_jmenem'][0].' ' : '';*/ ?></span>
                    <span class="member-name"><?php the_title(); ?></span>
                </a>
            </h4>
            <div class="member-lawyer"><?php echo $lawyer_info['person_funkce'][0]; ?></div>
            <table class="member-table">

                <?php if( !empty($lawyer_info['telefon'][0]) ) : ?>
                <tr>
                    <th>T:</th><td><a class="member-phone red-color sw-f-medium sw-contact-link"><?= render_maybe_obfuscated_email($lawyer_info['telefon'][0], 'tel:'); ?></a></td>
                </tr>
                <?php endif; ?>

                <?php if( !empty($lawyer_info['mobilni_telefon'][0]) ) : ?>
                <tr>
                    <th>M:</th><td><a class="member-phone red-color sw-f-medium sw-contact-link"><?= render_maybe_obfuscated_email($lawyer_info['mobilni_telefon'][0], 'tel:'); ?></a></td>
                </tr>
                <?php endif; ?>

                <?php if( !empty($lawyer_info['e-mail'][0]) ) : ?>
                <tr>
                    <th>E:</th><td><a class="member-mail red-color sw-f-medium sw-contact-link"><?= render_maybe_obfuscated_email($lawyer_info['e-mail'][0], 'mailto:'); ?></a></td>
                </tr>
                <?php endif; ?>

            </table>
        </div>
    </div>    
</div>