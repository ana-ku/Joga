<?php get_header(); ?>

<div class="single-news default-page-section align-center sw-line">
    <header class="text-center">
        <h1 class="red-color"><?= __( 'News', TEMPLATE_CTXT ); ?></h1>
    </header>
	<div href="<?php the_permalink(); ?>" class="reference-row news-row row narrow">

		<div class="content small-12 medium-12 large-12">
			<h2><?php the_title(); ?></h2>
            <span class="date"><?php echo get_the_date(); ?></span>
			<p class="entry-content"><?php echo get_the_content(); ?></p>
		</div>

	</div>
    <footer class="row narrow">
        <a href="<?= esc_url( get_page_link(icl_object_id(171, 'post', true)) ); ?>" class="button sw-button sw-section-button" style="margin-left: 0"><?php _e('Back to archive','grafiquex'); ?></a>
    </footer>
</div>

<div class="reference-section news-seciton">

	<?php // Get news
	/*$args_news = array(
		'post_type'		=> 'post',
		'post-status'	=> 'publish',
		"posts_per_page"=> -1,
		'post__not_in'		=> array(get_the_ID()),
	);
	$news = new WP_Query($args_news);
	$i = 0;
	while ($news->have_posts()): $news->the_post(); $i++; ?>			
		
		<a href="<?php the_permalink(); ?>" class="reference-row news-row row">
			<div class="portrait-left small-12 medium-2 large-2 columns" style="background-image: url('<?php echo the_post_thumbnail_url('large'); ?>')">
			</div>
			<div class="content small-12 medium-8 large-8 columns">
				<span class="date"><?php echo get_the_date(); ?></span>
				<h2><?php the_title(); ?></h2>
				<p class="entry-content"><?php echo limit_string(get_the_content(), 400); ?></p>
			</div>

			<div class="portrait-right small-12 medium-2 large-2 columns"></div>
		</a>

	<?php endwhile;*/ ?> 

<!--	</div>-->
</div>

<?php get_footer(); ?>