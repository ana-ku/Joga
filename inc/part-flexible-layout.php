<!-- Contact page entry content -->
<div class="flexible-content-section">               

<?php while( have_rows('flexible_content')): the_row();

    if( get_row_layout() == 'single_column' ): ?>
        
        <div class="row-section">
            <div class="row medium align-center">
                <div class="flexible entry-content small-12 columns <?php if(get_sub_field('odsazeni_obsahu1') == 'off') {echo 'no-padding';}?>">
                    <?php the_sub_field('single_column_content'); ?>
                </div>
            </div>
        </div>

    <?php elseif( get_row_layout() == 'two_columns' ): ?>


            <div class="row-section">
                <div class="row medium align-center">
                
                <?php if(get_sub_field('flexbile_content_as_bg1') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-6 columns <?php if(get_sub_field('odsazeni_obsahu1') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('left_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-6 columns <?php if(get_sub_field('odsazeni_obsahu1') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('left_column_content');?>   
                        </div>  

                <?php } ?>

                <?php if(get_sub_field('flexbile_content_as_bg2') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-6 columns <?php if(get_sub_field('odsazeni_obsahu2') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('right_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-6 columns <?php if(get_sub_field('odsazeni_obsahu2') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('right_column_content');?>   
                        </div>  

                <?php } ?>

                </div>
            </div>

    <?php elseif( get_row_layout() == 'three_columns' ): ?>

        <div class="row-section">
            <div class="row medium align-center ">

                <?php if(get_sub_field('flexbile_content_as_bg1') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-4 columns <?php if(get_sub_field('odsazeni_obsahu1') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('first_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-4 columns <?php if(get_sub_field('odsazeni_obsahu1') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('first_column_content');?>   
                        </div>  

                <?php } ?>

                <?php if(get_sub_field('flexbile_content_as_bg2') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-4 columns <?php if(get_sub_field('odsazeni_obsahu2') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('second_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-4 columns <?php if(get_sub_field('odsazeni_obsahu2') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('second_column_content');?>   
                        </div>  

                <?php } ?>

                <?php if(get_sub_field('flexbile_content_as_bg3') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-4 columns <?php if(get_sub_field('odsazeni_obsahu3') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('third_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-4 columns <?php if(get_sub_field('odsazeni_obsahu3') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('third_column_content');?>   
                        </div>  

                <?php } ?>
            </div>
        </div>

    <?php elseif( get_row_layout() == 'four_columns' ): ?>

        <div class="row-section"> 
            <div class="row medium align-center">
            
                <?php if(get_sub_field('flexbile_content_as_bg1') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-3 columns <?php if(get_sub_field('odsazeni_obsahu1') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('first_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-3 columns <?php if(get_sub_field('odsazeni_obsahu1') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('first_column_content');?>   
                        </div>  

                <?php } ?>

                <?php if(get_sub_field('flexbile_content_as_bg2') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-3 columns <?php if(get_sub_field('odsazeni_obsahu2') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('second_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-3 columns <?php if(get_sub_field('odsazeni_obsahu2') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('second_column_content');?>   
                        </div>  

                <?php } ?>

                <?php if(get_sub_field('flexbile_content_as_bg3') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-3 columns <?php if(get_sub_field('odsazeni_obsahu3') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('third_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-3 columns <?php if(get_sub_field('odsazeni_obsahu3') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('third_column_content');?>   
                        </div>  

                <?php } ?>

                <?php if(get_sub_field('flexbile_content_as_bg4') == 'on') { ?>

                    <div class="flexible entry-content small-12 medium-6 large-3 columns <?php if(get_sub_field('odsazeni_obsahu3') == 'off') {echo 'no-padding';} ?>" style="background-image: url(<?php echo get_content_image_url(get_sub_field('fourth_column_content')); ?>);">                
                    </div>  

                <?php } else { ?>               

                        <div class="flexible entry-content small-12 medium-6 large-3 columns <?php if(get_sub_field('odsazeni_obsahu4') == 'off') {echo 'no-padding';} ?>">
                            <?php the_sub_field('fourth_column_content');?>   
                        </div>  

                <?php } ?>

                </div>
            </div>
        </div>

    <?php endif;

endwhile; ?>

</div>