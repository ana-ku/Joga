<?php
/**
 * Template Part: Contact detailed info
 */
?>

<div class="member-card">
    <div class="member-basic-info">
        <?php include(locate_template( 'part-contact-info.php' )); ?>
    </div>
    <dl>
        <dt class="sw-f-medium"><?php _e('Education','grafiquex') ?></dt>
        <dd><?php echo !empty($lawyer_info['vzdelani'][0]) ? $lawyer_info['vzdelani'][0] : '-'; ?></dd>
        <dt class="sw-f-medium"><?php _e('Language skills','grafiquex') ?></dt>
        <dd><?php echo !empty($lawyer_info['jazykove_znalosti'][0]) ? $lawyer_info['jazykove_znalosti'][0] : '-'; ?></dd>
        <dt class="sw-f-medium"><?php _e('Specialization','grafiquex') ?></dt>
        <dd>
            <?php if(!empty($lawyer_info['specializace'][0])) {
    
                $specialization = maybe_unserialize($lawyer_info['specializace'][0]);
                $services = array();

                if(is_array($specialization)) {
                    foreach($specialization as $service_id) {
                        $services[] = '<a href="' . get_the_permalink( icl_object_id($service_id, 'post', true) ) .'" class="sw-link sw-link-underline">' . mb_strtolower(get_the_title( $service_id )) . '</a>';
                    }
                    echo implode( ', ', $services );
                } else {
                    echo '<a href="' . esc_url( get_page_link(icl_object_id(44, 'post', true)) ) .'?'. http_build_query(array('sn'=>$specialization)) .'" class="sw-link sw-link-underline">' . mb_strtolower(get_the_title( $specialization )) . '</a>';
                }
            } ?>
        </dd>
        <dt class="sw-f-medium"><?php _e('Professional membership','grafiquex') ?></dt>
        <dd><?php echo !empty($lawyer_info['profesni_clenstvi'][0]) ? $lawyer_info['profesni_clenstvi'][0] : '-'; ?></dd>
        <?php if ( ICL_LANGUAGE_CODE!='pl' ) : ?>
        <dt class="sw-f-medium"><?php _e('Other','grafiquex') ?></dt>
        <dd><?php echo $content; ?></dd>
        <?php endif; ?>
    </dl>
</div>