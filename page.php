<?php get_header(); ?>

<div class="default-page-section sw-line">
	<div class="row medium">
		<header class="page-header small-12 columns align-center align-middle text-center red-color">
			<h1 class="page-title"><?php the_title(); ?></h1>
		</header>
	</div>	

	<?php if (have_posts())  { ?>

		<div class="row narrow">

			<?php while (have_posts()): the_post(); ?>			
				
				<div class="entry-content content columns">
					<?php the_content(); ?>
				</div>

			<?php endwhile; ?>

		</div>

	<?php } ?>

</div>
		
<!-- Flexible layout -->
<?php get_template_part('inc/part-flexible-layout'); ?>

<?php get_footer(); ?>
