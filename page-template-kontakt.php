<?php
/**
 * Template Name: Kontakt
 */

get_header(); ?>

<!-- contact -->
<section id="contact-page" class="contact-narrow text-center sw-line">
    <header>
        <h1 class="red-color"><?= mb_strtoupper( get_the_title() ); ?></h1>
    </header>
    
    <div class="row">
        <div class="contact-data small-12 medium-6 large-6">
            <div class="row-company sw-block">
                <h3 class="red-color sw-f-regular"><?= get_field('nadpis_tabulky'); ?></h3>
                <span class="sw-mini-line red-background"></span>
            </div>
<!--
        <div class="row-frontman">
            <h4 class="red-color">Mgr. Daniel Siwy</h4>
            <span class="thinitalic brighter">advokát a insolvenční správce</span>
        </div>
-->

<br />

            
            <div class="row-data">
                <table class="">
                    <tr class="row-space">
                        <th><?= get_field('sidlo_nazev_radku'); ?></th>
                        <td>
                            <?= get_field('sidlo_adresa'); ?>
                            <br />
                            <img src="<?= get_template_directory_uri() . '/assets/img/vchod.png'; ?>" alt="vchod" />
                        </td>
                    </tr>
                    
                    <?php if( have_rows('kontaktni_udaje_jednotlive_udaje') ) : ?>
                        <?php while( have_rows('kontaktni_udaje_jednotlive_udaje') ) : the_row(); ?>
                    
                            <tr<?= get_sub_field('mezera') ? ' class="row-space"' : ''; ?>>
                                <th><?php the_sub_field('nazev_kontaktu'); ?></th>
                                <td>
                                    <?php switch( get_sub_field('typ_kontaktu') ) : case 'phone': ?>
                                            <span class="sw-link-underline red-color"><a href="tel:<?php the_sub_field('kontaktni_udaj'); ?>"><?php the_sub_field('kontaktni_udaj'); ?></a></span>
                                            <?php break; ?>
                                        <?php case 'mail': ?>
                                            <span class="sw-link-underline red-color"><?= render_maybe_obfuscated_email(get_sub_field('kontaktni_udaj'), 'mailto:'); ?></span>
                                            <?php break; ?>
                                        <?php default: ?>
                                            <?php the_sub_field('kontaktni_udaj'); ?>
                                            <?php break; ?>
                                    <?php endswitch; ?>
                                    &nbsp;
                                    <span class="sw-f-lightitalic"><?php the_sub_field('komentar'); ?></span>
                                </td>
                            </tr>
                    
                        <?php endwhile; ?>
                    <?php endif; ?>
                </table>
            </div>
        </div>
        <div class="building-entrance small-12 medium-6 large-6 text-center" style="background:linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5)), url(<?= get_template_directory_uri() . '/assets/img/budova.jpg'; ?>) no-repeat;background-position:bottom right;background-size:contain;">
        </div>
    </div>
</section><!-- /#contact-page -->

<!-- map -->
<section id="contact-page-map">
    <div id="googleMap" style="width:100%;height:430px;"></div>
    
    


    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSrBsK2FqSpOKX6AN0gT76OK7IhOfDMFo&callback=initMap"></script>
    <script>
        google.maps.event.addDomListener(window, 'load', init);
        google.maps.event.addDomListener(window, 'resize', init);
        
        function init() {
          var myLatlng = new google.maps.LatLng(49.749214, 18.626616);
          var mapOptions = {                      
              zoom: 16,
              scrollwheel: false,
              draggable: false,
              center: myLatlng, 
              styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#a7b2bb"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#a7b2bb"
      },
      {
        "visibility": "on"
      },
      {
        "weight": 3.5
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#a7b2bb"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "administrative.province",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#a7b2bb"
      },
      {
        "visibility": "on"
      },
      {
        "weight": 3.5
      }
    ]
  },
  {
    "featureType": "landscape",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "landscape.natural",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#ceeac6"
      },
      {
        "lightness": 35
      }
    ]
  },
  {
    "featureType": "landscape.natural.landcover",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#a7b2bb"
      }
    ]
  },
  {
    "featureType": "landscape.natural.terrain",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#a7b2bb"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#e4f3ff"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
          };
          
          
                    

          var markerImage = new google.maps.MarkerImage('<?= get_template_directory_uri(); ?>/assets/img/siwy_bubblee.png',
                            new google.maps.Size(89, 100), //size
                            new google.maps.Point(0, 0), //origin point
                            new google.maps.Point(68, 75));

          
          var mapElement = document.getElementById('googleMap');
          var map = new google.maps.Map(mapElement, mapOptions);
          var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              title: '<?php _e('SIWY & CO.') ?>',
              icon: markerImage
          });   
          
          marker.addListener('click', function() {        
            window.open( 'https://www.google.com/maps/place/Siwy+Daniel+Mgr./@49.7492864,18.6240951,17z/data=!4m13!1m7!3m6!1s0x47140400bf37a099:0xb976a9269b4de864!2zSGxhdm7DrSB0xZkuIDg3LzIsIDczNyAwMSDEjGVza8O9IFTEm8Whw61uLCBDemVjaGlh!3b1!8m2!3d49.749283!4d18.6262838!3m4!1s0x47140403df095561:0x9dca39c2d6874921!8m2!3d49.7492305!4d18.6263551?hl=en-US',
              '_blank'
            );                        
          });               
        }
    </script>
    
</section><!-- /#contact-page-map -->



<?php get_footer(); ?>