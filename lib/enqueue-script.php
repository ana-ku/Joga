<?php
// Enqueue your compiled CSS files here

if( !function_exists( 'reverie_enqueue_style' ) ) {
	function reverie_enqueue_style() {

		// Register Foundation CSS
//		wp_register_style( 'foundation', get_template_directory_uri() . '/assets/css/foundation.css', array(), '', 'all' );
		wp_register_style( 'foundation', get_template_directory_uri() . '/assets/css/foundation.my.mini.css', array(), '', 'all' );
	
		// Register custom CSS
		wp_register_style( 'stylesheet', get_template_directory_uri() . '/style.css', array(), '', 'all' );

    	wp_enqueue_style( 'foundation' );
		wp_enqueue_style( 'stylesheet' );
	}
}
add_action( 'wp_enqueue_scripts', 'reverie_enqueue_style' );


// Add admin CSS
function load_custom_wp_admin_style() {
    wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/admin.css', false);
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

?>