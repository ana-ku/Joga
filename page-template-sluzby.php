<?php
/**
 * Template Name: Specializace
 */

get_header(); ?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/font-awesome.min.css">

<!-- services -->
<?php // Get services
$args_services = array(
    'post_type'		=> SERVICES_PT,
    'post-status'	=> 'publish',
    'order'         => 'ASC',
    "posts_per_page"=> -1
);
$services = new WP_Query($args_services);

// Get targeted service
$target = isset($_GET['sn']) ? $_GET['sn'] : '1'; ?>

<?php if($services->have_posts()) : ?>
<div id="services-section" class="row collapse">
    <div class="medium-3 columns">
        <h2><?php echo mb_strtoupper(__('Specialization','grafiquex')); ?></h2>
        <ul class="vertical tabs" data-tabs id="services-tabs">
            <?php $index = 0; ?>
            <?php while ($services->have_posts()): $services->the_post(); $index++; ?>
                <?php $key_contacts = maybe_unserialize(get_post_meta( get_the_ID(), 'klicovy_kontakt', true ));
                $key_contacts = is_array($key_contacts) ? implode(',', $key_contacts) : $key_contacts; ?>
                <li class="tabs-title sw-f-light <?= $target==get_the_ID() ? 'is-active' : ''; ?>">
                    <a href="#panel<?=the_ID()?>" contacts="<?=$key_contacts?>" sid="<?=the_ID()?>" <?= $index==1 ? 'aria-selected="true"' : ''; ?>><?php the_title(); ?></a>
                </li>
            <?php endwhile; ?>
        </ul>
    </div>
    <div class="medium-9 columns">
        <div class="tabs-content" data-tabs-content="services-tabs">
            <?php $index = 0; ?>
            <?php while ($services->have_posts()): $services->the_post(); $index++; ?>
                <div class="tabs-panel <?= $target==get_the_ID() ? 'is-active' : ''; ?>" id="panel<?php the_ID(); ?>">
                    <h3 class="red-color"><?php echo $index . '/ ' . mb_strtoupper(get_the_title()); ?></h3>
                    <p><?php the_content(); ?></p>
                </div>
            <?php endwhile; ?>
        </div>
        
        <a href="<?= esc_url( get_page_link(icl_object_id(38, 'post', true)) ); ?>" class="button sw-button-secondary"><?php _e('Contact us','grafiquex'); ?></a>
    </div>
</div>
<?php endif; ?>

<!-- key contact -->
<?php // Get all lawyers
$args_lawyers = array(
    'post_type'		=> LAWYER_PT,
    'post-status'	=> 'publish',
    'order'         => 'ASC',
    "posts_per_page"=> -1
);
$lawyers = new WP_Query($args_lawyers); ?>

<?php $key_contacts = get_post_meta( 56, 'klicovy_kontakt', true ); ?>
<?php if($lawyers->have_posts() && !empty($key_contacts)) : ?>
<section id="key-contact" class="text-center align-center">
    <div class="sw-line"></div>
    <h2 class="red-color"><?php echo mb_strtoupper( __('Key contact','grafiquex') ); ?></h2>
    <div class="row narrow">
        <?php while ($lawyers->have_posts()): $lawyers->the_post(); ?>
            <?php if(in_array(get_the_ID(), $key_contacts)) {
                $display = '';
            } else {
                $display = 'display:none;';
            }
            $lawyer_info = get_post_meta(get_the_ID()); ?>
            <div class="member extra-small-1 small-6 medium-6 large-6 columns small-centered" lid="<?=the_ID()?>" style="<?=$display;?>">
                <?php include(locate_template( 'part-contact-info.php' )); ?>
            </div>
        <?php endwhile; ?>
    </div>
    
    <a href="<?= esc_url( get_page_link(icl_object_id(47, 'post', true)) ); ?>" class="button sw-button sw-section-button"><?php _e('Go to whole team','grafiquex'); ?></a>
</section>
<?php endif; ?>

<?php get_footer(); ?>