<?php
require_once('config.php');
require_once('lib/clean.php');
require_once('lib/enqueue-style.php'); // Enqueue styles here
require_once('lib/foundation.php'); // Load Foundation specific functions like top-bar
require_once('lib/nav.php'); // Filter default wordpress menu classes and clean wp_nav_menu markup


/**
 *	Create "Website settings" admin option page
 */
if ( function_exists('acf_add_options_page') ) {	
	acf_add_options_page(array(
		'page_title' 	=> 'Nastavení stránky',
		'menu_title'	=> 'Nastavení stránky',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true,
		'position' 		=> '3.1',
	));
}

/**
 * Edit role capibilities
 */
// get the the role object
$role_object = get_role("editor");

// add $cap capability to this role object
$role_object->add_cap("edit_theme_options");
$role_object->add_cap("gform_full_access");
$role_object->add_cap("wpml_manage_string_translation");
$role_object->add_cap("wpml_manage_translation_management");

/**
 * Add Zalomeni plugin also to ACF fields
 */
function my_acf_format_value( $value, $post_id, $field ) {	
	// run do_shortcode on all textarea values
	$value = do_shortcode($value);		
    
    global $wpZalomeni;

    // return
    if (method_exists($wpZalomeni, "texturize")) {
	    return Zalomeni::texturize($value);
    } else {
        return false;
    }
}
add_filter('acf/format_value', 'my_acf_format_value', 10, 3);


/**
 * Return custom WPML language switcher
 */
function get_custom_wpml_language_switcher($hide_active = false, $flags = false, $type = 'translated_name') {
    if (function_exists('icl_get_languages')) {
        $languages = icl_get_languages('skip_missing=0');
        $output = '';
        if(1 < count($languages)) {

            if ($has_dropdown == true) {
                $output .= "<ul class=\"wpml-language-switcher dropdown menu\" data-dropdown-menu>";
            } else {
                $output .= "<ul class=\"wpml-language-switcher menu\">";
            }        
            
            if ($hide_active == true) {        

                foreach($languages as $l ){
                    if(!$l['active']) {
                        $output .= "<li class=\"wpml-language-item\">";
                        $output .= '<a href="'.$l['url'].'">';
                        if ($flags == true) {
                            $output .= '<img src="' . $l['country_flag_url'] . '" alt="' . $l['language_code'] . __(" flag", TEMPLATE_CTXT) . '" />';  
                        }
                        $output .= $l[$type];
                        $output .= '</a>';
                    } 
                }

            } else {

                foreach($languages as $l ){
                    $output .= "<li class=\"wpml-language-item\">";
                    $output .= '<a href="'.$l['url'].'">';
                    if ($flags == true) {
                        $output .= '<img src="' . $l['country_flag_url'] . '" alt="' . $l['language_code'] . __(" flag", TEMPLATE_CTXT) . '" />';  
                    }
                    $output .= $l[$type];
                    $output .= '</a>';
                }

            }
            $output .= "</ul>";

            return $output;
        }
    } else {
        return 0;
    }      
}
/**
 * Return custom WPML language switcher - modified
 */
function get_custom_wpml_language_switcher_modified($hide_active = false, $flags = false, $type = 'translated_name') {
    
    if (function_exists('icl_get_languages')) {
        $languages = icl_get_languages('skip_missing=0');
        $output_array = array();
        $output = '';
        if(1 < count($languages)) {

            if ($has_dropdown == true) {
                $output .= "<ul class=\"wpml-language-switcher dropdown menu align-right\" data-dropdown-menu>";
            } else {
                $output .= "<ul class=\"wpml-language-switcher menu align-right\">";
            }
            
            if ($hide_active == true) {        

                foreach($languages as $l ){
                    $lang_output = '';
                    $current_l = $l['active']==1 ? 'class="lang-selected"' : '';
                    if(!$l['active']) {
                        $lang_output .= "<li class=\"wpml-language-item\">";
                        $lang_output .= '<a href="'.$l['url'].'" '.$current_l.'>';
                        if ($flags == true) {
                            $lang_output .= '<img src="' . $l['country_flag_url'] . '" alt="' . $l['language_code'] . __(" flag", TEMPLATE_CTXT) . '" />';  
                        }
                        $lang_output .= $l[$type];
                        $lang_output .= '</a></li>';
                        $output_array[] = $lang_output;
                    } 
                }

            } else {

                foreach($languages as $l ){
                    $lang_output = '';
                    $current_l = $l['active']==1 ? 'class="lang-selected"' : '';
                    $lang_output .= "<li class=\"wpml-language-item\">";
                    $lang_output .= '<a href="'.$l['url'].'" '.$current_l.'>';
                    if ($flags == true) {
                        $lang_output .= '<img src="' . $l['country_flag_url'] . '" alt="' . $l['language_code'] . __(" flag", TEMPLATE_CTXT) . '" />';  
                    }
                    $lang_output .= $l[$type];
                    $lang_output .= '</a></li>';
                    $output_array[] = $lang_output;
                }

            }
            
            // Add delimiter between languages & add to output string
            $output_string = implode('<li class="wpml-language-item"><span class="lang-delimiter">|</span></li>', $output_array);
            $output .= $output_string;
            
            $output .= "</ul>";

            return $output;
        }
    } else {
        return 0;
    }      
}
/**
 * Return custom WPML language switcher - modified (as select)
 */
function get_custom_wpml_language_switcher_modified_select($hide_active = false, $flags = false, $type = 'translated_name') {
    
    if (function_exists('icl_get_languages')) {
        $languages = icl_get_languages('skip_missing=0');
        $output_array = array();
        $output = '';
        if(1 < count($languages)) {            
            
            if ($hide_active == true) {        

                foreach($languages as $l ){
                    if( $l['active']==1 ) {
                        $output = "<a href=\"#\" class=\"wpml-language-switcher dropdown menu\" data-dropdown-menu>".$l[$type]."</a><ul class=\"sub-menu\">" . $output;
                    } else {
                        $lang_output = '';
                        if(!$l['active']) {
                            $lang_output .= "<li class=\"wpml-language-item menu-item\">";
                            $lang_output .= '<a href="'.$l['url'].'" '.$current_l.'>';
                            if ($flags == true) {
                                $lang_output .= '<img src="' . $l['country_flag_url'] . '" alt="' . $l['language_code'] . __(" flag", TEMPLATE_CTXT) . '" />';  
                            }
                            $lang_output .= $l[$type];
                            $lang_output .= '</a></li>';
                            $output_array[] = $lang_output;
                        }
                    }
                }

            } else {

                foreach($languages as $l ){
                    if( $l['active']==1 ) {
                        $output = "<a href=\"#\" class=\"wpml-language-switcher dropdown menu\" data-dropdown-menu>".$l[$type]."</a><ul class=\"sub-menu\">" . $output;
                    } else {
                        $lang_output = '';
                        $lang_output .= "<li class=\"wpml-language-item menu-item\">";
                        $lang_output .= '<a href="'.$l['url'].'" '.$current_l.'>';
                        if ($flags == true) {
                            $lang_output .= '<img src="' . $l['country_flag_url'] . '" alt="' . $l['language_code'] . __(" flag", TEMPLATE_CTXT) . '" />';  
                        }
                        $lang_output .= $l[$type];
                        $lang_output .= '</a></li>';
                        $output_array[] = $lang_output;
                    }
                }

            }
            
            // Add delimiter between languages & add to output string
            $output_string = implode('', $output_array);
            $output .= $output_string;
            
            $output .= '</ul>';

            return $output;
        }
    } else {
        return 0;
    }      
}

/**
 * Disable unneccesary WPML ballast
 */
define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);

/**
 * Moves Navigation link from Appearance to main menu
 */
add_action('admin_menu', 'change_menus_position');
function change_menus_position() {

    // Remove old menu
    remove_submenu_page( 'themes.php', 'nav-menus.php' );

    //Add new menu page
     add_menu_page(
       'Navigace',
       'Navigace',
       'edit_theme_options',
       'nav-menus.php',
       '',
       'dashicons-list-view',
       3
    );
}


/**
 * Move YOAST SEO metabox in admin to bottom
 */
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

/**
 * Edit role capibilities
 */
$role_object = get_role("editor");

$role_object->add_cap("edit_theme_options");
$role_object->add_cap("gform_full_access");
$role_object->add_cap("wpml_manage_string_translation");

/**
 * Get URL of image placed in WYSIWYG editor
 * => used in flexible content-layout
 */
function get_content_image_url($data){
    preg_match("/src=([^\\s]+)/", $data, $result);
    $result = $result[1];   
    $result = str_replace('"', '', $result); 
    return $result;
}

/**
 * Custom adding class to menu items
 */
function custom_menu_item_classes( $classes, $item, $args ) {
	if( (is_tax('tax-portfolio-category') ) && 'Realizace' == $item->title )
		$classes[] = 'active';

	return array_unique( $classes );
}
add_filter( 'nav_menu_css_class', 'custom_menu_item_classes', 10, 3 );


/**
 * Edit excerpt
 * 
 * --> @edited (20 -> 40)
 */
// Length
function siwy_excerpt_length($length) {
	return 40;
}
add_filter('excerpt_length', 'siwy_excerpt_length');

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 *
 * --> @edited
 */
function edit_excerpt_more($more) {
    return '...<a class="arrow-right orange red-color" href="' . get_the_permalink() . '">'. __("pokračovat") . '</a>';
}
add_filter( 'excerpt_more', 'edit_excerpt_more' );

/**
 * Limit words fn
 */
function limit_string($string, $limit = 100) {
    // Return early if the string is already shorter than the limit
    if(strlen($string) < $limit) {return $string;}

    $regex = "/(.{1,$limit})\b/";
    preg_match($regex, $string, $matches);
    return $matches[1];
}

/**
 * Register features for this theme
 */
if(!function_exists('fm_wp_template_support')) {
    function fm_wp_template_support() {
        
        load_theme_textdomain(TEMPLATE_CTXT, get_template_directory() . 'lib/lang');  
        add_theme_support('post-thumbnails'); 
        add_theme_support('automatic-feed-links');          
        add_theme_support('menus');
        register_nav_menus(array(
            'primary' => __('Hlavní navigace', TEMPLATE_CTXT),
            'additional' => __('Vedlejší navigace', TEMPLATE_CTXT),
            'utility' => __('Pomocná navigace', TEMPLATE_CTXT)
        ));    
    }
}
add_action('after_setup_theme', 'fm_wp_template_support');

/**
 * Gravity forms field label visibility (in admin)
 */
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/**
 * Gravity forms - filter the button type
 */
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<input type='submit' id='gform_submit_button_{$form['id']}' class='gform_button button sw-button-tertiary form-show-last' value='".__('Send message',TEMPLATE_CTXT)."'>";
}

/**
 * Custom post types
 */
// Post type slug constants
define('LAWYER_PT','siwy_lawyer');
define('SERVICES_PT','siwy_services');

add_action( 'init', 'siwy_create_custom_post_types' );
function siwy_create_custom_post_types() {
    
    // Siwy Lawyer (Právníci)
    $labels = array(
        'name'                  => __('Právníci',TEMPLATE_CTXT),
        'singular_name'         => __('Právník',TEMPLATE_CTXT),
        'rewrite'               => array('slug' => 'osobnosti-advokatni-kancelare'),
        'add_new'               => __('Nový právník',TEMPLATE_CTXT),
        'add_new_item'          => __('Vytvořit nový profil právníka',TEMPLATE_CTXT),
        'edit_item'             => __('Editovat právníka',TEMPLATE_CTXT),
        'new_item'              => __('Nový právník', TEMPLATE_CTXT),
        'view_item'             => __('Detail právníka', TEMPLATE_CTXT),
        'search_items'          => __('Vyhledat právníka', TEMPLATE_CTXT),
        'not_found'             => __('Nenalezen žádný profil právníka', TEMPLATE_CTXT),
        'not_found_in_trash'    => __('V koši nenalezen žádný profil právníka', TEMPLATE_CTXT),
        'parent_item_colon'     => '',
    );
    
    register_post_type( LAWYER_PT, array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'exclude_from_search'   => true,
        'query_var'             => true,
        'rewrite'               => array('slug' => 'siwy_lawyer'),
        'capability_type'       => 'post',
        'has_archive'           => true,
        'hierarchical'          => false,
        'menu_position'         => 10,
        'menu_icon'             => 'dashicons-groups',
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' )
    ) );
    
    
    // Siwy Services (Služby/Specializace)
    $labels = array(
        'name'                  => __('Specializace',TEMPLATE_CTXT),
        'singular_name'         => __('Specializace',TEMPLATE_CTXT),
        'add_new'               => __('Nová specializace',TEMPLATE_CTXT),
        'add_new_item'          => __('Vytvořit novou specializaci',TEMPLATE_CTXT),
        'edit_item'             => __('Editovat specializaci',TEMPLATE_CTXT),
        'new_item'              => __('Nová specializace', TEMPLATE_CTXT),
        'view_item'             => __('Detail specializace', TEMPLATE_CTXT),
        'search_items'          => __('Vyhledat specializaci', TEMPLATE_CTXT),
        'not_found'             => __('Nenalezena žádná specializace', TEMPLATE_CTXT),
        'not_found_in_trash'    => __('V koši nenalezena žádná specializace', TEMPLATE_CTXT),
        'parent_item_colon'     => '',
    );

    register_post_type( SERVICES_PT, array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'exclude_from_search'   => true,
        'query_var'             => true,
        'rewrite'               => array('slug' => 'specializace'),
        'capability_type'       => 'post',
        'has_archive'           => false,
        'hierarchical'          => false,
        'menu_position'         => 10,
        'menu_icon'             => 'dashicons-hammer',
        'supports'              => array( 'title', 'editor', 'custom-fields', 'revisions' )
    ) );
}

/**
 * Custom post type - custom columns and content
 */
add_filter( 'manage_posts_columns', 'siwy_lawyers_custom_column' );
add_action( 'manage_posts_custom_column', 'siwy_lawyers_custom_column_content', 10, 2 );

function siwy_lawyers_custom_column( $defaults ) {
    // Add custom column for featured image
    $defaults['siwy_featured_image'] = __('Fotka', TEMPLATE_CTXT);
    
    return $defaults;
}

function siwy_lawyers_custom_column_content( $column_name, $post_ID ) {
    // Check current column id
    if ($column_name == 'siwy_featured_image') {
        $post_featured_image = siwy_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" style="max-width:70px;" />';
        }
    }
}

// Just helper function for getting featured image for custom post type - lawyers
function siwy_get_featured_image($post_ID) {
    
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
        
        return $post_thumbnail_img[0];
    }
}

/**
 * Register sidebar
 */
add_action( 'widgets_init', 'register_sw_sidebar' );
function register_sw_sidebar() {
    $args = array(
        'name'          => __( 'Specializace', 'grafiquex' ),
        'id'            => 'sw_services_sidebar',
        'description'   => __( 'Sidebar určený pro detail specializace', TEMPLATE_CTXT ),
        'class'         => '',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>' );
    
    register_sidebar( $args );
}

/**
 * Register navigation menu
 */
add_action( 'after_setup_theme', 'register_secondary_menu' );
function register_secondary_menu() {
    register_nav_menu( 'sw_services_secondary', __( 'Vertikální menu na detailu specializace', TEMPLATE_CTXT ) );
}

/**
 * Just shortcut for email obfuscate
 *
 * If plugin 'Email Obfuscate Shortcode' active, renders 'secured' contact element
 */
function render_maybe_obfuscated_email( $contact_with_spaces, $action = '' ) {
    
    // Do nothing, if contact not given
    if ( empty($contact_with_spaces) ) return;
    
    // Initialize
    $secured_contact = $contact_with_spaces;
    $contact = str_replace(' ', '', $contact_with_spaces);
    $contact = str_replace('&nbsp;', '', $contact_with_spaces);
    
    // Email obfuctace function
    if(function_exists('eos_obfuscate')) {
        
        $secured_contact = eos_obfuscate( array( 'email' => str_replace('&nbsp;', ' ', $contact_with_spaces), 'linkable' => '0', 'noscript_message'=>__('Abyste mohli vidět toto pole, povolte, prosím, JavaScript.','grafiquex') ) );
    }
    
    return '<a href="'.$action.$contact.'">'.$secured_contact.'</a>';
}


/**
 * Limits given string.
 *
 * Finds last occurrence of the $needle before $limit position.
 * If string termination given, will be concatenated to the end.
 *
 * @param string    Optional. $string The string in which it is searched.
 * @param string    Optional. $needle The string, that is being searched for.
 * @param int       Optional. $limit The maximum position where to search.
 * @param string    Optional. $termination String that will be concatenated to the end of the shortened string.
 *
 * @return          Shortened string.
 */
function fmwp_limit_given_string( $string = '', $needle = '', $limit = 0, $termination = '' ) {
    
    // Initialize
    $lastPos = $lastOccur = 0;

    // Check if item is longer than limit
    if ( strlen($string) > $limit ) {
        // Get shortened last item
        while (($lastPos = strpos($string, $needle, $lastPos))!== false && $lastPos <= $limit) {
            $lastOccur = $lastPos;
            $lastPos = $lastPos + strlen($needle);
        }
        $result = substr( $string, 0, $lastOccur ) . $termination;
    } else {
        $result = $string;
    }
    
    return $result;
} ?>