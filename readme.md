# SIWY & CO. theme
### Based on Foundation 6

WordPress theme based on [ZURB Foundation](http://foundation.zurb.com/sites/download.html/) 6.4 flex-box framework. Template has cleaned tag to get rid of all unnecessary stuff. Menu walker is slightly edited.

#### Tags
Foundation 6, CSS

#### Description
Theme structure:
* Directory `assets` contains fonts, images, JS, CSS
* Directory `inc` includes flexible layout template
* Directory `lib` is for parts of code to include, e.g. helper functions, foundation specific functions, ...