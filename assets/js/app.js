(function($) {
    
    var _button_spinner = '<button class="button sw-button-tertiary" disabled="disabled"><div class="gform-spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></button>';
    
    // Initialize Foundation
    $(document).foundation();
    
    // On document ready
    $(document).ready(function() {
        
        // Show whole contact form
        $('.show-form').click(function(e,param) {
            
            var $input = $(this).find('input');
            
            // Change input padding + background (icon) & placeholder
            $input.css({'padding-left':'20px','background':'#fff'});
            $input.attr('placeholder',sw_script_vars.contact_form_name);

            var form = $(this).closest('form');

            // Set default placeholder color via removing form class
            form.removeClass('form-part');

            // Show whole form
            form.find('.form-show').slideDown('slow',function() {
                form.find('.form-show-last').fadeIn();
            });
            
            // If param true, go to form after showing whole form
            if( param ) {
                moveToContent('#question', 1000);
            }
        });
        
        // Better move to targeted element, because of fixed header
        if (window.location.hash) {
            var elem_id = window.location.hash.substring(1);
            
            if(elem_id) {
                moveToContent("#"+elem_id);
            }
        }
        
        // Move to confirm/error message after contact form is submitted and page is reloaded - for nonAJAX forms
        if ( $('#question .gform_confirmation_wrapper').length>0 ) {
            moveToContent('#question',1500);
        }
        if ( $('#question .validation_error').length>0 ) {
            var function_after = function() {
                // Trigger click the form and focus
                $('#question form ul li:first-child input').trigger('click');
                $('#question form ul li:first-child input').focus();
            };
            
            moveToContent('#question',1500,function_after);
        }
        
        // Proper height of intro section
        if ( $('#intro').length>0 ) {
            setProperIntroHeight();
        }
    });
    
    $(window).resize(function() {
        // Proper height of intro section
        if ( $('#intro').length>0 ) {
            setProperIntroHeight();
        }
    });
    
    function setProperIntroHeight() {
        var windowHeight = $(window).height(),
            windowWidth = $(window).width(),
            windowRatio = windowHeight/windowWidth,
            menuHeight = 200,
            newHeight = windowHeight;
        
        //console.log(windowWidth,windowHeight);
        
        // Menu adaptation
        if ( windowWidth < 640 ) {
            menuHeight = 75;
        }
        
        // Min height
        if ( windowHeight < 270 && windowWidth < 640 ) {
            newHeight = 270;
        } else if ( windowHeight < 400 && windowWidth >= 640  ) {
            newHeight = 400;
        } else {
            if ( windowRatio < 0.4 ) {
                // too wide and small

            } else if ( windowRatio > 1.65 ) {
                // too tall
                newHeight = windowWidth * 1.5;
                //console.log('too tall, ratio:',windowRatio,'new height:',newHeight);
            }
        }

        $('#intro').css({'padding':0,'height':newHeight+menuHeight+'px'});
    }
    
    // Move to mission section after continue button clicked
    $('.sw-continue').click(function() {
        moveToContent('#mission');
    });
    
    // Show spinner button when form is submitted
    $('#question .gform_button[type="submit"]').click(function() {
        // Hide submit button
        $(this).hide();
        // Add spinner
        $(this).after(_button_spinner);
        // Disable text inputs
        $(this).closest('form').find('input[type="text"], textarea.textarea').prop('readonly',true);
    });
    
    // Move to confirm message after contact form is submitted - only for AJAX forms
    $(document).on("gform_confirmation_loaded", function (e, form_id) {
        moveToContent('#question');
    });
    
    // Move to top
    $('#sw-scroll-to-top').click(function(e) {
        e.preventDefault();
        
        moveToContent('#wrapper',1500);
    });
    
    // Sticky menu
    $('.title-bar').on('sticky.zf.stuckto:top', function(){
        $(this).addClass('shrink');
    }).on('sticky.zf.unstuckfrom:top', function(){
        $(this).removeClass('shrink');
    });
    
    // Show/hide corresponding key contacts on service page
    $('#services-tabs>li>a').click(function() {
        // Get lawyers as string
        var lawyers = $(this).attr('contacts'),
            sid = $(this).attr('sid'),
            url = window.location.search;
        
        // Deep linking
        deep = replaceQueryParam('sn', sid, url);
        history.pushState(null, '', deep);
        
        // Some lawyers set?
        if(lawyers) {
            // Get lawyers as array
            lawyers = lawyers.split(',');
            
            // Show key contact section
            $('#key-contact').fadeIn();
            
            // Hide all lawyers and show corresponding lawyers
            $('#key-contact .member').hide();
            for(var i=0; i<lawyers.length; i++) {
                $('#key-contact .member[lid="'+lawyers[i]+'"]').fadeIn();
            }
        } else {
            // If no specific lawyers set, hide section
            $('#key-contact').slideUp('fast');
        }
        
    });
    
    // Intro and menu background breathing only on reload
    $('#intro').addClass('fit');
    $('#responsive-menu').addClass('fit');
    
    // Move to question section after "Contact us" button clicked
    $('.sw-trigger-contact-form').click(function(e) {
        e.preventDefault();
//        var test = 'ahoj';
        
//        var function_after = function() {
            // Trigger click the form and focus
            $('#question form ul li:first-child input').trigger('click',true);
            $('#question form ul li:first-child input').focus();
//        };
        
//        moveToContent('#question', 1000, function_after);
    });
    
    
    /* FUNCTIONS */
    function replaceQueryParam(param, newval, search) {
        var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
        var query = search.replace(regex, "$1").replace(/&$/, '');

        return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
    }
    
    // Move to content (if selector given)
    function moveToContent(selector,duration,after) {
        var $elem = $(selector),
            adminbarHeight = $('#wpadminbar').outerHeight();
        
        duration = duration || 500;
        after = after || function(){};
        
        if( $elem.length>0 ) {
            $('html, body').animate({
                scrollTop: $elem.offset().top - (adminbarHeight+54) //  54 = 58 menu height - 4 section line height
            }, duration, 'swing', after);
        }
    }
    
})(jQuery);