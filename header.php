<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <title><?php wp_title('|', true, 'right'); ?></title>

    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
    
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/cbg-grayscale.css">

    <?php wp_head(); ?>
</head>

<body id="top" <?php body_class('antialiased'); ?>>

<!-- OC -->
<div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <!-- OC menu -->
        <div class="off-canvas position-right sw-f-light" id="offCanvasPrimary" data-position="right" data-off-canvas="offCanvasPrimary">
                    
            <!-- Close navicon -->
            <span class="title-bar navicon-cont" type="button" data-toggle="offCanvasPrimary" data-responsive-toggle="responsive-menu" data-hide-for="medium">
                <span class="navicon navicon-close"></span>
            </span>
            
            <?php $is_service = is_singular(SERVICES_PT) ? 'service-active ' : ''; ?>
            <?php // Primary topbar menu constructor
                wp_nav_menu( array(
                    'theme_location'        => 'primary',
                    'container'             => false,
                    'depth'                 => 0,
                    'items_wrap'            => '<ul class="'.$is_service.'oc-menu vertical menu"data-accordion-menu>%3$s</ul>',
                    'fallback_cb'           => 'fmwp_menu_fallback', // workaround to show a message to set up a menu
                    'walker'                => new FMWP_walker( 
                        array(
                            'in_top_bar'            => true,
                            'item_type'             => 'li',
                            'menu_type'             => 'main-menu'
                        ) 
                    ),
                ) ); ?>

            <div class="langs">
                <?php // Output WPML language switcher
                echo get_custom_wpml_language_switcher_modified(false, false, 'tag'); ?>
            </div>
            
        </div>

        <!-- OC content -->
        <div id="wrapper" class="off-canvas-content" data-off-canvas-content>
            <main class="container" role="main">
                <header class="header">
                    <div id="mobile-menu" class="show-for-small-only">
                        <!-- Logo -->
                        <a class="site-title-link site-title-link-mobile" href="<?= esc_url( home_url( '/' ) ); ?>" title="<?= esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" aria-label="<?= esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                            <div class="site-logo-mobile"></div>
                        </a>
                        <!-- Navicon -->
                        <span class="title-bar navicon-cont" type="button" data-toggle="offCanvasPrimary" data-responsive-toggle="responsive-menu" data-hide-for="medium">
                            <span class="navicon"></span>
                        </span>
                    </div>
                    
                    <div data-sticky-container class="hide-for-small-only">
                        <div id="responsive-menu" class="title-bar" data-top-anchor="50" data-sticky data-options="marginTop:0;">
                            <div class="title-bar-left show-for-medium" style="max-width:380px;">
                                <span class="title-bar-title">
                                    <a class="site-title-link" href="<?= esc_url( home_url( '/' ) ); ?>" title="<?= esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                                        <img class="site-logo" src="<?= get_template_directory_uri('') ?>/assets/img/logo_siwy.png" alt="<?php bloginfo( 'name' ); ?>" />
                                        <img class="site-logo-small" src="<?= get_template_directory_uri('') ?>/assets/img/logo_siwy_initials.png" alt="<?php bloginfo( 'name' ); ?>" />
                                    </a>
                                </span>
                            </div>
                            
                            <div class="title-bar-right">
                                <div class="langs">
                                    <?php // Output WPML language switcher
                                    echo get_custom_wpml_language_switcher_modified(false, false, 'tag'); ?>
                                </div>
                                <!-- Primary navigation -->
                                <nav class="primary-navigation hide-for-small">
                                    <?php wp_nav_menu(
                                        array(
                                            'theme_location'        => 'primary',
                                            'container'             => false,
                                            'depth'                 => 0,
                                            'items_wrap'            => '<ul class="'.$is_service.'dropdown menu align-right" data-dropdown-menu>%3$s</ul>',
                                            'after'                 => '<div class="menu-item-border"></div>',
                                            'fallback_cb'           => 'fmwp_menu_fallback',
                                            'walker'                => new FMWP_walker( 
                                                array(
                                                    'in_top_bar'            => true,
                                                    'item_type'             => 'li',
                                                    'menu_type'             => 'main-menu'
                                                ) 
                                            ),
                                        )
                                    ); ?>
                                </nav>
                                <div class="langs-select sw-f-light">
                                    <?php // Output WPML language switcher
                                    echo get_custom_wpml_language_switcher_modified_select(false, false, 'tag'); ?>
                                </div>
                            </div>
                            
                            <div class="title-bar-background <?= is_front_page() ? 'fp' : '';  ?>"></div>
                            <div class="title-bar-shrink-overlay <?= is_front_page() ? 'fp' : '';  ?>"></div>
                        </div>
                    </div>
                    
                </header>