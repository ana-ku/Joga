<?php get_header(); ?>

<div class="reference-section">
	<div class="row medium">
		<header class="page-header small-12 columns align-center align-middle">
			<h1 class="page-title"><?php the_title(); ?></h1>
		</header>
	</div>	

	<?php 
	$i = 0;
	while (have_posts()): the_post(); $i++; ?>			
		
		<div class="reference-row row medium">
			<div class="portrait-left small-12 medium-2 large-3 columns" style="background-image: url('<?php echo the_post_thumbnail_url(); ?>')"></div>

			<div class="content small-12 medium-8 large-6 columns">
				<h2><?php the_title(); ?></h2>
				<?php echo limit_string(get_the_content(), 400); ?>
			</div>

			<div class="portrait-right small-12 medium-2 large-3 columns"></div>
		</div>

	<?php endwhile; ?> 

	</div>
</div>
		
<?php get_footer(); ?>
