<?php
/**
 * Template Name: Kontakt
 */

get_header(); ?>

<!-- contact -->
<section id="contact-page" class="text-center sw-line">
    <header>
        <h1 class="red-color"><?php echo mb_strtoupper( __('Kontakt',TEMPLATE_CTXT) ); ?></h1>
    </header>
    
    <div class="row">
        <div class="contact-data small-12 medium-6 large-6">
            <div class="row-company sw-block">
                <h3 class="red-color sw-f-regular">Advokátní kancelář SIWY & CO. s.r.o.</h3>
                <span class="sw-mini-line red-background"></span>
            </div>
<!--
        <div class="row-frontman">
            <h4 class="red-color">Mgr. Daniel Siwy</h4>
            <span class="thinitalic brighter">advokát a insolvenční správce</span>
        </div>
-->

<br />

            
            <div class="row-data">
                <table class="">
                    <tr class="row-space">
                        <th>Sídlo:</th>
                        <td><?php // Get address, add link after first line and echo string with <br> before new line
                            echo nl2br(get_theme_mod( 'siwy_address_setting', '' )); ?> <span class="sw-f-light brighter"><a href="<?= esc_url( get_page_link(47) ); ?>" class="sw-link"><?php _e('(mapa)',TEMPLATE_CTXT) ?></a></span>
                            <div class="sw-f-lightitalic sw-link"><?php _e( '(vchod do administrativní části je z&nbsp;ulice Masarykovy sady)' ); ?></div>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>Tel:</th>
                        <td class="sw-link-underline red-color"><?= get_theme_mod( 'siwy_phone_setting', '' ); ?></td>
                    </tr>
                    <tr>
                        <th>Fax:</th>
                        <td class="sw-link-underline red-color"><?= get_theme_mod( 'siwy_fax_setting', '' ); ?></td>
                    </tr>
                    <tr class="row-space">
                        <th>E-mail:</th>
                        <td class="sw-link-underline red-color"><?php $email = get_theme_mod( 'siwy_email_setting', '' );
                            if(function_exists(‘eos_obfuscate’)) {
                                echo eos_obfuscate(array(’email’ => $email));
                            } else {
                                echo $email;
                            } ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>IČ:</th>
                        <td class="sw-link-underline"><?= get_theme_mod( 'siwy_ic_setting', '' )?></td>
                    </tr>
                    <tr class="row-space">
                        <th>DIČ:</th>
                        <td class="sw-link-underline"><?= get_theme_mod( 'siwy_dic_setting', '' )?></td>
                    </tr>
                    
                    <tr>
                        <th>ID DS:</th>
                        <td><?= get_theme_mod( 'siwy_idds1_setting', '' ); ?> <span class="sw-f-lightitalic"><?php _e('(datová schránka)',TEMPLATE_CTXT); ?></span></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="building-entrance small-12 medium-6 large-6 text-center" style="background:linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5)), url(<?= home_url( '/wp-content/uploads/2017/09/Budova.jpg' ) ?>) no-repeat bottom right;background-size:contain;">
        </div>
    </div>
</section><!-- /#contact-page -->

<!-- map -->
<section id="contact-page-map">
    <div id="googleMap" style="width:100%;height:430px;"></div>
    
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        google.maps.event.addDomListener(window, 'load', init);
        google.maps.event.addDomListener(window, 'resize', init);
        
        function init() {
          var myLatlng = new google.maps.LatLng(49.749214, 18.626616);
          var mapOptions = {                      
              zoom: 17,
              scrollwheel: false,
              draggable: false,
              center: myLatlng, 
              styles: [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#fff"},{"saturation":-100},{"gamma":2.15},{"lightness":10}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}]
          };

          var markerImage = new google.maps.MarkerImage('<?= get_template_directory_uri(); ?>/assets/img/siwy_bubble.png',
                            new google.maps.Size(392, 113), //size
                            new google.maps.Point(0, 0), //origin point
                            new google.maps.Point(200, 80));

          
          var mapElement = document.getElementById('googleMap');
          var map = new google.maps.Map(mapElement, mapOptions);
          var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              title: '<?php _e('SIWY & CO.') ?>',
              icon: markerImage
          });   
          
          marker.addListener('click', function() {        
            window.open( 'https://www.google.com/maps/place/Siwy+Daniel+Mgr./@49.7492864,18.6240951,17z/data=!4m13!1m7!3m6!1s0x47140400bf37a099:0xb976a9269b4de864!2zSGxhdm7DrSB0xZkuIDg3LzIsIDczNyAwMSDEjGVza8O9IFTEm8Whw61uLCBDemVjaGlh!3b1!8m2!3d49.749283!4d18.6262838!3m4!1s0x47140403df095561:0x9dca39c2d6874921!8m2!3d49.7492305!4d18.6263551?hl=en-US',
              '_blank'
            );                        
          });               
        }
    </script>
    
</section><!-- /#contact-page-map -->

<!--


<?php // Get all lawyers
$args_lawyers = array(
    'post_type'		=> LAWYER_PT,
    'post-status'	=> 'publish',
    'order'         => 'ASC',
    "posts_per_page"=> -1
);
$lawyers = new WP_Query($args_lawyers); ?>

<?php if($lawyers->have_posts()) : ?>
<section id="contact-page-team" class="member-cards text-center align-center sw-line">
    <header>
        <h2 class="red-color"><?= mb_strtoupper( __('Náš tým',TEMPLATE_CTXT) ); ?></h2>
    </header>
    
    <div class="univerzal-overlay"></div>

    <div class="row narrow align-center">
        <?php while ($lawyers->have_posts()): $lawyers->the_post(); ?>
            <?php $lawyer_info = get_post_meta(get_the_ID()); ?>
            <div class="member extra-small-1 small-12 medium-6 large-6 columns">
                <?php include(locate_template( 'part-contact-info.php' )); ?>
            </div>
        <?php endwhile; ?>
    </div>

    <h3 class="red-color we-are-ready"><?= mb_strtoupper( __('We are ready to take your time',TEMPLATE_CTXT) ); ?></h3>

    <footer>
        <a href="<?= esc_url( get_page_link(47) ); ?>" class="button sw-button sw-section-button"><?php _e('Zobrazit celý tým',TEMPLATE_CTXT); ?></a>
    </footer>
</section>

<?php endif; ?>-->

<?php get_footer(); ?>