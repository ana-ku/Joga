<?php
/**
 * Template Name: Tým
 */

get_header(); ?>

<!-- all lawyers -->
<?php // Get all lawyers
$args_lawyers = array(
    'post_type'		=> LAWYER_PT,
    'post-status'	=> 'publish',
    'order'         => 'ASC',
    "posts_per_page"=> -1
);
$lawyers = new WP_Query($args_lawyers); ?>

<?php if($lawyers->have_posts()) : ?>
<section id="whole-team" class="member-cards text-center align-center sw-line">
    <header>
        <h1 class="red-color"><?= mb_strtoupper( __('Our team','grafiquex') ); ?></h1>
    </header>
    
    <div class="univerzal-overlay"></div>
    
    <div class="row narrow">
        <?php while ($lawyers->have_posts()): $lawyers->the_post(); ?>
            <?php $content = get_the_content();
            $lawyer_info = get_post_meta( get_the_ID() ); ?>
            <div id="<?= sanitize_title( get_the_title(), '' ); ?>" class="member small-12 medium-6 large-6 columns text-left">
                <?php include(locate_template( 'part-contact-detailed-info.php' )); ?>
            </div>
        <?php endwhile; ?>
    </div>
    
<!--     <h3 class="red-color we-are-ready"><?= mb_strtoupper( __('We are ready to take your time','grafiquex') ); ?></h3> -->
    
    <footer>

<!--         <a href="<?= esc_url( get_page_link(icl_object_id(38, 'post', true)) ); ?>" class="button sw-button sw-section-button"><?php _e('Contact us','grafiquex'); ?></a> -->
        
        <a href="#" class="button sw-button-secondary sw-trigger-contact-form"><?php _e('Contact us','grafiquex'); ?></a>
        
        
    </footer>
</section><!-- /#whole-team -->
<?php endif; ?>

<?php get_footer(); ?>