<?php get_header(); ?>

    <section id="services-section" class="row">

        <?php get_sidebar(); ?>

        <div class="small-12  large-9 columns service-content" role="main">

        <?php while (have_posts()) : the_post(); ?>
            <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                <header>
                    <h3 class="red-color"><?php echo /* get_post_meta( get_the_ID(), 'poradi_specializace', true ) .*/  '' . mb_strtoupper(get_the_title()); ?></h3>
                </header>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
                <footer>
                    <?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', TEMPLATE_CTXT), 'after' => '</p></nav>' )); ?>
                    <p class="entry-tags"><?php the_tags(); ?></p>
<!--                     <?php edit_post_link('Edit this Post'); ?> -->
                    <a href="#" class="button sw-button-secondary sw-trigger-contact-form"><?php _e('Contact us','grafiquex'); ?></a>
                </footer>
            </article>
        <?php endwhile; ?>

        <?php wp_reset_postdata(); ?>

        </div>

    </section><!-- /#services-sectio -->

    <!-- key contact -->
    <?php $key_contacts = get_post_meta( get_the_ID(), 'klicovy_kontakt', true ); ?>

    <?php // Get key lawyers
    $args_lawyers = array(
        'post_type'		=> LAWYER_PT,
        'post-status'	=> 'publish',
        'order'         => 'ASC',
        'post__in'      => $key_contacts,
        "posts_per_page"=> -1
    );
    $lawyers = new WP_Query($args_lawyers); ?>

    <?php if($lawyers->have_posts() && !empty($key_contacts)) : ?>
    <section id="key-contact" class="member-cards text-center align-center sw-line">
        <header>
            <h2 class="red-color"><?= mb_strtoupper( __('Key contact','grafiquex') ); ?></h2>
        </header>
        
        <div class="univerzal-overlay"></div>

        <div class="row narrow">
            <?php while ($lawyers->have_posts()): $lawyers->the_post(); ?>
                <?php $lawyer_info = get_post_meta(get_the_ID()); ?>
                    <div class="member extra-small-1 small-12 medium-6 large-6 columns grid-centered">
                        <?php include(locate_template( 'part-contact-info.php' )); ?>
                    </div>
            <?php endwhile; ?>
        </div>

        <footer>
            <a href="<?= esc_url( get_page_link(icl_object_id(47, 'post', true)) ); ?>" class="button sw-button sw-section-button"><?php _e('Go to whole team','grafiquex'); ?></a>
        </footer>
    </section><!-- /#key-contact -->
    <?php endif; ?>

    <?php wp_reset_postdata(); ?>
		
<?php get_footer(); ?>