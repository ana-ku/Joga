                </main><!-- /.container -->

            <!-- question -->
            <section id="question" class="text-center align-center">
                <h3 class="section-title-big"><?php _e('Do you have any questions? Do&nbsp;not&nbsp;hesitate&nbsp;to&nbsp;contact&nbsp;us.','grafiquex'); ?></h3>
                <h3 class="section-title-small"><?php _e('Do you have any questions? Do not hesitate to contact us.','grafiquex'); ?></h3>
                <div class="row narrow align-center">
                    <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
                </div>
            </section>

            <footer id="footer">
                <div class="footer-narrow">
                    <a href="#" id="sw-scroll-to-top" aria-hidden="true"></a>
                    <div class="row row-company sw-block"><span class="sw-f-medium"><?= get_field('paticka_nadpis_paticky', 'option'); ?></span><span class="sw-mini-line"></span></div>
                    <div class="row row-frontman">
                    </div>
                    <div class="row row-company-info">
                        <div class="small-12 medium-6 large-4 column">
                            <?php // Get address, add link after first line and echo string with <br> before new line
                            $contact_link = '<span class="sw-f-light brighter"><a href="'.esc_url( get_page_link(icl_object_id(38, 'post', true)) ).'" class="sw-link">'.__('(contacts)','grafiquex').'</a></span>';
                            $address = get_field('paticka_kontaktni_udaje_adresa', 'option');
                            $address = substr_replace($address, ' '.$contact_link, strpos($address,"\n")-strlen("\n"),strlen("\n"));
                            echo nl2br($address); ?>
                        </div>
                        <div class="small-12 medium-6 large-4 column">
                            <table class="footer-table">
                                <tr>
                                    <th><?php _e('Tel','grafiquex'); ?>:</th>
                                    <td class="sw-contact-link"><?= render_maybe_obfuscated_email(get_field('paticka_kontaktni_udaje_telefon', 'option'),'tel:'); ?></td>
                                </tr>
                                <tr>
                                    <th><?php _e('Fax','grafiquex'); ?>:</th>
                                    <td><?= get_field('paticka_kontaktni_udaje_fax', 'option'); ?></td>
                                </tr>
                                <tr>
                                    <th><?php _e('E-mail','grafiquex'); ?>:</th>
                                    <td class="sw-contact-link"><?= render_maybe_obfuscated_email(get_field('paticka_kontaktni_udaje_e-mail', 'option'), 'mailto:'); ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="small-12 medium-6 large-4 column">
                            <table class="footer-table">
                                <tr>
                                    <th><?php _e('ID no.','grafiquex'); ?>:</th>
                                    <td><?= get_field('paticka_kontaktni_udaje_ic', 'option'); ?></td>
                                </tr>
                                <tr>
                                    <th><?php _e('Tax reg. no.','grafiquex'); ?>:</th>
                                    <td><?= get_field('paticka_kontaktni_udaje_dic', 'option'); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row row-copyright">
                        <span class="thin-small sw-f-light darker"><?= get_field('paticka_copyright_text', 'option'); ?></span>
                    </div>

                    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/font-awesome.min.css">
                </div>
            </footer>

        </div><!-- /#wrapper -->
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>