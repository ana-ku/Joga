<?php get_header(); ?>

<div class="search-results-section default-page-section">
    <div class="row medium">
        <header class="page-header small-12 columns align-center align-middle text-center red-color">
            <h1 class="page-title"><?php _e( 'Výsledky vyhledávání pro', TEMPLATE_CTXT ); ?> "<?php echo get_search_query(); ?>"</h1>
        </header>
    </div>
    
    <div class="row narrow">

        <!-- Search results -->
        <div class="search-results small-12 columns">       

        <?php if ( have_posts() ) : // If some posts available
            while ( have_posts() ) : the_post(); // Posts loop ?>
                
                <article id="post-<?php the_ID(); ?>" <?php post_class('index-card'); ?>>
                    <header>
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php //grafiquex_entry_meta(); ?>
                    </header>
                    <div class="entry-content">
                        <figure><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('large'); } ?></a></figure> <?php the_excerpt(); ?>
                    </div>
                </article>
            
            <?php endwhile; // End of posts loop
        else : // If no posts ?>

            <article id="post-0" class="post no-results not-found">
                <header>
                    <h2><?php _e( 'Nic nenalezeno', TEMPLATE_CTXT ); ?></h2>
                </header>
                <div class="entry-content">
                    <p><?php _e( 'Omlouváme se, ale pro zadaný dotaz nebyly nalezeny žádné výsledky.', TEMPLATE_CTXT ); ?></p>
                </div>
                <hr />
                <br>
                <p><?php _e( 'Můžete zkusit následující:', TEMPLATE_CTXT ); ?></p>
                <ul> 
                    <li><?php _e( 'Zkontrolujte název stárnky', TEMPLATE_CTXT ); ?></li>
                    <li><?php printf( __( 'Zkuste <a href="%s" class="red-color">hlavní stránku</a>', TEMPLATE_CTXT ), home_url() ); ?></li>
                    <li><?php printf( __( '<a href="%s" class="red-color sw-trigger-contact-form">Kontaktujte nás</a>', TEMPLATE_CTXT ), '#' ); ?></li>
                </ul>
            </article>
            
        <?php endif; // End of posts if ?>
        
        <?php // Display pagination if needed
        if ( function_exists('grafiquex_pagination') ) { grafiquex_pagination(); } else if ( is_paged() ) { ?>
            <nav id="post-nav">
                <div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'grafiquex' ) ); ?></div>
                <div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'grafiquex' ) ); ?></div>
            </nav>
        <?php } ?>

        </div><!-- /.archive-posts -->

    </div>
</div><!-- /.archive-section -->


<!--
<div class="404-section default-page-section sw-line">
    
	<div class="row narrow">	

		<div class="entry-content content 404-cont small-12 large-8 columns">			
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="main-content">
					<div class="error">
						<p class="bottom"><?php _e( "Stránka neexistuje nebo je aktuálně nedostupná.", TEMPLATE_CTXT ); ?></p>
					</div>
					<p><?php _e( 'Můžete zkusit následující:', TEMPLATE_CTXT ); ?></p>
					<ul> 
						<li><?php _e( 'Zkontrolujte název stárnky', TEMPLATE_CTXT ); ?></li>
						<li><?php printf( __( 'Zkuste <a href="%s">hlavní stránku</a>', TEMPLATE_CTXT ), home_url() ); ?></li>
						<li><?php printf( __( '<a href="%s" class="sw-trigger-contact-form">Kontaktujte nás</a>', TEMPLATE_CTXT ), '#' ); ?></li>
					</ul>
				</div>
			</article>
		</div>

	</div>
</div>
-->
		
<?php get_footer(); ?>