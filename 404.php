<?php get_header(); ?>

<div class="404-section default-page-section">
    
    <div class="row medium">
        <header class="page-header small-12 columns align-center align-middle text-center red-color">
            <h1 class="page-title"><?php _e( 'Page not found', 'grafiquex' ); ?></h1>
        </header>
    </div>
    
	<div class="row narrow">	

		<!-- Main 404 content -->
		<div class="entry-content content 404-cont small-12 large-8 columns">			
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="main-content">
					<div class="error">
						<p class="bottom"><?php _e( "Page doesn't exist or is not currently unavailable.", 'grafiquex' ); ?></p>
					</div>
					<p><?php _e( 'You can try following:', 'grafiquex' ); ?></p>
					<ul> 
						<li><?php _e( 'Check you spelling', 'grafiquex' ); ?></li>
						<li><?php printf( __( 'Go to <a href="%s" class="red-color">home page</a>', 'grafiquex' ), home_url() ); ?></li>
						<li><?php printf( __( '<a href="%s" class="red-color sw-trigger-contact-form">Contact us</a>', 'grafiquex' ), '#' ); ?></li>
					</ul>
				</div>
			</article>
		</div><!-- /.404-cont -->

	</div>
</div><!-- /.404-section -->
		
<?php get_footer(); ?>